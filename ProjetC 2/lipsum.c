#include "lipsum.h"


void lipsum_generator(int Number, char choice)
{
    int RandInt, words;

    char mot[LEN_MOT];

    srand(SEED);

    if (choice == 'w') // On affiche que le nombre de mots choisi
    {
        for (int m = 0; m < Number; m++)
        {
            RandInt = rand() % LEN_DIC;
            strcpy_s(mot, LEN_MOT, Dict[RandInt]);
            printf("%s ", mot);

        } // fin for
        printf("\n");
    }

    else if (choice == 'p') // On affiche le nombre de paragraphe choisi avec un nombre de mots al�atoire mais pas trop grand et de taille minimale
    {
        for (int i = 0; i < Number; i++)
        {
            words = rand() % 30 + 10;

            for (int m = 0; m < words; m++)
            {
                RandInt = rand() % LEN_DIC;
                strcpy_s(mot, LEN_MOT, Dict[RandInt]);
                printf("%s ", mot);
            }
            printf("\n\n");

        }
        printf("\n");
    }
    return;

}

int main(int argc, char* argv[])
{
    int Number = atoi(argv[2]);

    lipsum_generator(Number, argv[1][1]);

return EXIT_SUCCESS;
}