#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tagChecker.h"
//#include "lipsum.h"

int main(int argc, char* argv[])
{
    Bool ok = TRUE;
    char strErrOpen[LEN_STR];

    FILE* htmlPtr;

    printf("Fichier : %s\n", argv[1]);

    if (argc != 2) 
    {
        fprintf(stderr, "Nombre d'arguments incorrect\n");
        return EXIT_FAILURE;
    }

    int fopen_code = fopen_s(&htmlPtr, argv[1], "r");

    if (fopen_code != 0)
    {
        strerror_s(strErrOpen, LEN_STR, errno);
        fprintf(stderr, "Erreur : %s\n", strErrOpen);
        return EXIT_FAILURE;
    }

    // On v�rifie pour toutes les balises connues

    ok = ok && !tagChecker(htmlPtr, "<strong>");
    ok = ok && !tagChecker(htmlPtr, "<p>");
    ok = ok && !tagChecker(htmlPtr, "<h1>");
    ok = ok && !tagChecker(htmlPtr, "<h2>");
    ok = ok && !tagChecker(htmlPtr, "<a>");

    if (ok) printf("Le fichier HTML est conforme\n");
    else
    {
        printf("Le fichier HTML n'est pas conforme\n");
        return EXIT_FAILURE;
    }

    fclose(htmlPtr);
    return EXIT_SUCCESS;
}