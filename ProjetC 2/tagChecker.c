#include "tagChecker.h"

Bool tagChecker(FILE* fileptr, char Tag[]) // Le format des balises est <tag> </tag> le tag </tag> est sous une forme unique contrairement au <tag> d�clinable en <tag qqch>
{
    FILE* filelog;

    fopen_s(&filelog, "html_checker.log", "w");

    int count = 0;
    int caractere = ' ';
    int Nblignes = 1;

    char myString[LEN_STR];

    char tag[LEN_TAG];

    int len_Tag = strlen(Tag);

    strncpy_s(tag, LEN_TAG, Tag, len_Tag - 1);

    tag[len_Tag] = '\0';

    char _tag[LEN_TAG];

    _tag[0] = '<';
    _tag[1] = '/';

    for (int i = 2; i < len_Tag; i++)
        _tag[i] = tag[i - 1];

    _tag[len_Tag] = '>';
    _tag[len_Tag + 1] = '\0';

    fseek(fileptr, 0, SEEK_SET);

    while (caractere != EOF)
    {
        caractere = fgetc(fileptr);
        if (caractere == '\n') Nblignes++;
    }

    fseek(fileptr, 0, SEEK_SET);

    int len1 = strlen(tag);
    int len2 = strlen(_tag);

    fprintf(filelog, "Les tags sont : %s & %s\n", tag, _tag);

    for (int i = 0; i < Nblignes; i++)
    {
        fgets(myString, LEN_STR, fileptr); // On r�cup�re la ligne qu'il faut

        fprintf(filelog, "\n La chaine de caractere lue %s � la ligne %d ==> \n", myString, i);

        char* tagResult = strstr(myString, tag); // On extrait la balise ouvrante de la chaine de caract�res

        if (tagResult != NULL)
        {
            count++;
            fprintf(filelog, "%s(%d), ", tagResult, count);
        }

        char* _tagResult = strstr(myString, _tag); // On extrait la balise fermante de la chaine de caract�res

        if (_tagResult != NULL)
        {
            count--;
            fprintf(filelog, "%s(%d), ", _tagResult, count);
        }
    }

    fprintf(filelog, "\n\nLe resulat du decompte des balises %s> et %s est : %d\n", tag, _tag, count);

    if (count != 0)
    {
        fprintf(stderr, "Les balises %s> et %s sont en nombre incorrect\n", tag, _tag);
        fprintf(filelog, "Sortie : Format html incorrect ==> les balises %s> et %s sont en nombre incorrect\n", tag, _tag);
        fclose(filelog);
        return EXIT_FAILURE;
    }

    fclose(filelog);
    return EXIT_SUCCESS;
}