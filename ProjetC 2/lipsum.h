#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define LEN_DIC 100
#define LEN_MOT 64
#define SEED time(NULL)

void lipsum_generator(int Number, char choice);

// On d�finit le dictionnaire des mots latins

char *Dict[] = { "Lorem","ipsum","dolor","sit","amet","consectetur","adipiscing","elit","Maecenas","nec","felis","et","eros","vulputate","auctor","Vivamus","sollicitudin","massa","dolor","vitae","pellentesque","eros","pharetra","at","Donec","faucibus","cursus","mi","Etiam","ut","sem","lectus","Etiam","at","erat","id","diam","elementum","vulputate","ac","non","lectus",
"Proin","et","est","venenatis","odio","faucibus","volutpat","vitae","quis","velit","Curabitur","nec","mauris","sed","lectus","venenatis","auctor","Nullam","id","massa","ultricies","porta","orci","ut","imperdiet","dolor","Nunc","eget","est","dolor","Nunc","et","sagittis","ipsum","nec","pretium","ligula","Vestibulum","ante","est","egestas","vel","ex","in","malesuada","elementum","tellus","Nam","ut","lectus","ut","nisi","tempus","semper","ut","eget","eros","Morbi" };