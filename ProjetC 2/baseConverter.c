#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

void baseConverter(int defaultBase, char *numTarget ){

    int binStr[16];
    double binTab[16];
    
    int num, saveNum;
    double res = 0;
    
    switch (defaultBase)
    {
        case 10:
            num=atoi(numTarget);
            saveNum = num; //sauvegarde du nombre pour affichage hexa
            int i = 0;
            while (num > 0) // conversion (10)=>(2)
            {
                binStr[i] = num % 2;
                num = num / 2;
                i++;
            }

            for (int j = i-1; j >=0 ; j--) //boucle d'affichage du nombre en binaire
            {
                printf("%d", binStr[j]);
            }

            printf(" (2), %X (16)\n", saveNum); //affichage hexa
            break;

        case 2:

            for (int i = 0; i < strlen(numTarget); i++) //permet de passer d'un array de char vers un array de int {1,0,1,0}
            {
                if (*(numTarget + i)=='1')
                    binTab[i]=1;
                else
                    binTab[i]=0;
            }

            for (int i = 0; i < strlen(numTarget); i++) //conversion (2)=>(10)
            {
                res += binTab[(strlen(numTarget)-1)-i] * pow((double)2,(double)i);
            }
            
            printf("%0.f (10), %X (16)\n", res, (int)res); //conversion (2)=>(10) et (10)=>(16)
            break;

        case 16:

            for (int i = 0; i < strlen(numTarget); i++) //conversion des termes alpha numerique en numerique
            {
                switch (numTarget[i]){
                    case 'A':
                        binStr[i] = 10;
                        break;
                    case 'B':
                        binStr[i] = 11;
                        break;
                    case 'C':
                        binStr[i] = 12;
                        break;
                    case 'D':
                        binStr[i] = 13;
                        break;
                    case 'E':
                        binStr[i] = 14;
                        break;
                    case 'F':
                        binStr[i] = 15;
                        break;
                    default:
                        binStr[i] = atoi(numTarget + i);
                        break;
                }
            }

            for (int i = 0; i < strlen(numTarget); i++) //conversion (16)=>(10)
            {
                res += binStr[(strlen(numTarget)-1)-i] * pow((double)16, (double)i);
            }

            num = (int)res;

            while (num > 0) //conversion (10)=>(2)
            {
                binStr[i] = num % 2;
                num = num / 2;
                i++;
            }

            for (int j = i-1; j >=0 ; j--) //affichage binaire
            {
                printf("%d", binStr[j]);
            }
            printf(" (2), %0.f (10)", res);
    }
}

int main(int argc, char **argv){

    int opt, base;
    char target[10];
    strcpy(target, argv[1]);

    while((opt = getopt(argc, argv, ":n:b:"))!=-1){ //gestion des options avec getopt
        switch (opt)
        {
        case 'n':
            strcpy(target, optarg);
            break;
        case 'b':
            base = atoi(optarg);
            break;

        default:
            break;
        }
    }
    
    baseConverter(base, target); //appel de la fonction de conversion

    return 0;
}