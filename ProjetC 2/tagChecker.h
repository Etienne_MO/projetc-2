#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LEN_STR 255
#define LEN_TAG 64

#define TRUE 1
#define FALSE 0

typedef int Bool;

Bool tagChecker(FILE * fileptr, char Tag[]);


