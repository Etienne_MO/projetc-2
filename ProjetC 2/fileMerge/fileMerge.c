/*
Programme fusionnant (concaténant) et "défusionnant" des fichiers. Le programme prendra par la ligne de commande le noms de fichiers à fusionner. Sortie: fichier unique créé par la fusion des fichiers pris en entrée
Le programme, par une option -u, devra "unmerge" le fichier

Trouver un moyen de stocker dans le fichier de sortie, les informations vous permettant de "défusionner" les fichiers par la suite.
Des fichiers de différents types pourront être fusionnés.

Exemples d'utilisation de la commande:
file_merge file1.png file2.png file3.pdf => output: fichier files.merge
file_merge -u files.merge => output: fichiers file1.png file2.png file3.pdf 
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void string2hexString(char* input, char* output)
{
    int loop = 0;
    int i = 0;

    while (input[loop] != '\0') {
        sprintf((char*)(output + i), "%02X", input[loop]);
        loop += 1;
        i += 2;
    }
    //insert NULL at the end of the output string
    output[i++] = '\0';
}

typedef struct fileMergeUnmerge
{
    char files[25][25];
    enum {
        merge,
        unmerge
    }action;
}mergeOrUnmerge;

void action(int filesCount, mergeOrUnmerge nameF){
    FILE *merge, *file;
        for (int i = 0 ; i < filesCount ; i++ ){
        printf("--in action---  %s \n", nameF.files[i]);
    }
    int type = nameF.action;
    if (type == 0 ){
        puts("merge in action");
        //char hex_Name[(strlen(fileName)*2)+1];
        //merge = fopen("files.merge","wb");
        // Début boucle for sur files
        
        //file = fopen("file","rb");
        //transformation du nom du fichier en hexa pour le fputc
        //string2hexString(FileName, hex_Name);
        // fputc(0x21 0x64 0x65 0x62 merge) // !deb en hex
        // Début boucle lecture file
        if (file != NULL)
        {
            while (!feof(file))
            {
                // copie file dans le merge 
                char buffer = fgetc(file);
                fputc(buffer, merge);
            }
        }
        // Fin boucle lecture file
        //fputc(0x21 0x66 0x69 0x6e) // !fin en hex
        fclose(file);
        // Fin boucle for sur files
        fclose(merge);
    }else{
        puts("unmerge in action");
        unsigned char buffer, deb1, deb2, deb3, deb4, fin1, fin2, fin3, fin4;
        char *nomFile;
        int pos =0;
        merge = fopen("files.merge","rb");

        if (merge != NULL)
        {
            while (!feof(merge))
            {
                //extraction
                // 4 datas pour regarder 4 bits si correspond a !deb ou !fin
                fseek(merge, pos, SEEK_SET);    deb1 = fgetc(merge);      pos++;
                fseek(merge, pos, SEEK_SET);    deb2 = fgetc(merge);      pos++;
                fseek(merge, pos, SEEK_SET);    deb3 = fgetc(merge);      pos++;
                fseek(merge, pos, SEEK_SET);    deb4 = fgetc(merge);      pos++;
                if( file != NULL){ // si fichier ouvert

                    // regarde 4 prochain
                    fseek(merge, pos, SEEK_SET);    fin1 = fgetc(merge);   pos++;
                    fseek(merge, pos, SEEK_SET);    fin2 = fgetc(merge);   pos++;
                    fseek(merge, pos, SEEK_SET);    fin3 = fgetc(merge);   pos++;
                    fseek(merge, pos, SEEK_SET);    fin4 = fgetc(merge);   pos++;
                    
                    if (fin1 == 0x21 && fin2 == 0x64 && fin3 ==0x65 && fin4 ==0x62){
                        // reset deb1->4
                        deb1 = "";  deb2 = "";  deb3 = "";  deb4 = "";                        
                        fclose(file);
                    }else{
                        fputc(fin1, file);
                        fputc(fin2, file);
                        fputc(fin3, file);
                        fputc(fin4, file);
                    }   
                }else{
                    if (deb1 == 0x21 && deb2 == 0x66 && deb3 ==0x66 && deb4 ==0x6e ){ //si 4 permier == !deb 
                        file = fopen("fichier","wb");
                    }else{
                        strcat(nomFile,deb1);
                        pos -=3; // reviens 3 position en arriere 1er boucle pos = 4 => pose = 1 
                    }                    
                }
            }
        }
        fclose(merge);
    }
}

int main(int argc, char **argv){

    // gcc file -u f1 f2 f3
    
    int nb, nbFichier = argc - 2;
    //mergeOrUnmerge *liste = malloc(sizeof(liste));

    mergeOrUnmerge liste;

    //*liste.files = calloc(argc-1, sizeof(char)*255);

    for(nb = 1; nb < argc; nb++) {

        char *argument = argv[nb];

        if (nb == 1){

            if (strcmp(argument,"-m") == 0){
                liste.action = merge;
            }else{
                liste.action = unmerge;
            }
        }
        else{
            strcpy_s(liste.files[nb-2], 255, argv[nb]);
        }
    }
    action(nbFichier, liste);

    //printf("action dans liste->action %d \n", liste->action);
    for (int i = 0 ; i < nbFichier ; i++ ){
        //printf("-----  %s \n", liste.files[i]);
    }

    return 0;
}
